use actix_web::{get, post, web, HttpResponse, Responder};
use crate::structure::operands::Operands;

#[get("")]
pub async fn multiplication_via_get_method(operands: web::Query<Operands>) -> impl Responder {
    HttpResponse::Ok().body(format!("[via get] {} * {} = {}", operands.a, operands.b, operands.a * operands.b))
}

#[post("")]
pub async fn multiplication_via_post_method(operands: web::Json<Operands>) -> impl Responder {
    HttpResponse::Ok().body(format!("[via post] {} * {} = {}", operands.a, operands.b, operands.a * operands.b))
}