use actix_web::{get, web, HttpResponse, Responder};
use crate::structure::operands::Operands;
use crate::service::history;

#[get("")]
pub async fn division_via_get_method(operands: web::Query<Operands>) -> impl Responder {
    
    return match calculate_and_persist(operands.a, operands.b).await {
        Ok(calcul) => HttpResponse::Ok().body(format!("[via get] {calcul}")),
        Err(err) => HttpResponse::InternalServerError().body(err),
    };
}

pub async fn calculate_and_persist(a: u8, b: u8) -> Result<String, &'static str> {
    return match calculate(a, b) {
        Ok(result) => {
            let operation: String = format!("{a} / {b} = {result}");
    
            history::bread::add(operation.clone())?;

            return Ok(operation);
        },
        Err(error) => Err(error),
    };
}

pub fn calculate(a: u8, b: u8) -> Result<u8, &'static str> {
    if b == 0 {
        return Err("Division by zero is not allowed");
    }

    let result: u8 = a / b;

    return Ok(result);
}