use serde::Deserialize;

#[derive(Deserialize)]
pub struct Operands {
    pub a: u8,
    pub b: u8,
}