use actix_web::{web, Scope};

use crate::action::{
    addition::*,
    division::*,
    multiplication::*,
    substraction::*
};

pub fn routes() -> Scope {
    web::scope("")

    .service(web::scope("/addition")
        .service(addition_via_get_method)
        .service(addition_via_post_method)
    )
    .service(web::scope("/division")
        .service(division_via_get_method)
    )
    .service(web::scope("/multiplication")
        .service(multiplication_via_get_method)
        .service(multiplication_via_post_method)
    )
    .service(web::scope("/substraction")
        .service(substraction_via_get_method)
        .service(substraction_via_post_method)
    )
}