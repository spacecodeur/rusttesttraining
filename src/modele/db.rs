use dotenvy::dotenv;
use diesel::prelude::*;

use crate::utils::dotenv::get_env_var;

pub fn get_connection() -> SqliteConnection {

    dotenv().ok();

    let database_url: String = get_env_var("DATABASE_URL");

    SqliteConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}