use diesel::Insertable;
use crate::modele::schema::history;

#[derive(Debug, Insertable)]
#[diesel(table_name = history)]
pub struct NewHistory {
    pub operation: String,
}

#[derive(diesel::prelude::Queryable)]
pub struct History {
    pub id: i32,
    pub operation: String,
}
