use diesel::table;

table! {
    history (id) {
        id -> Integer,
        operation -> VarChar,
    }
}