use crate::action::division::calculate;

#[test]
fn test_unit_calculate_success() {
    let result = calculate(10, 2);
    assert_eq!(result, Ok(5));
}

#[test]
fn test_unit_calculate_division_by_zero() {
    let result = calculate(10, 0);
    assert_eq!(result, Err("Division by zero is not allowed"));
}

#[test]
fn test_unit_calculate_max_values() {
    let result = calculate(255, 1);
    assert_eq!(result, Ok(255));

    let result = calculate(255, 255);
    assert_eq!(result, Ok(1));
}

#[test]
fn test_unit_calculate_zero_dividend() {
    let result = calculate(0, 5);
    assert_eq!(result, Ok(0));
}