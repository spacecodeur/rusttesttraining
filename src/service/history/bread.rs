use diesel::prelude::*;
use crate::modele::db::get_connection;
use crate::modele::structure;
use crate::modele::schema;

pub fn add(operation: String) -> Result<(), &'static str> {
    
    let conn: &mut SqliteConnection = &mut get_connection();
    
    let new_history: structure::history::NewHistory = structure::history::NewHistory {
        operation,
    };

    match diesel::insert_into(schema::history::table)
        .values(&new_history)
        .execute(conn){
            Ok(_) => Ok(()),
            Err(_) => Err("Error during insert into DB"),
        }
}

pub fn browse() -> Result<Vec<structure::history::History>, &'static str> {
    use crate::modele::schema::history::dsl::*;

    let conn: &mut SqliteConnection = &mut get_connection();

    match history.load::<structure::history::History>(conn) {
        Ok(entries) => Ok(entries),
        Err(_) => Err("Error during fetching data from DB"),
    }
}