// @generated automatically by Diesel CLI.

diesel::table! {
    history (id) {
        id -> Nullable<Integer>,
        operation -> Text,
    }
}
