use actix_web::test;
use calculator::modele::structure::history::History;
use calculator::action::division::calculate_and_persist;
use calculator::service::history;

#[test]
async fn test_integ_calculate_and_persist() {

    let result: Result<String, &str> = calculate_and_persist(10, 2).await;
    assert!(result.is_ok());

    let histories: Vec<History> = history::bread::browse().unwrap();

    let last_history = histories.last().unwrap();
    assert_eq!(last_history.operation, "10 / 2 = 5");
}

#[test]
async fn test_integ_calculate_and_persist_when_division_by_zero() {

    let result: Result<String, &str> = calculate_and_persist(10, 0).await;
    assert_eq!(result, Err("Division by zero is not allowed"));

    let histories: Vec<History> = history::bread::browse().unwrap();

    let last_history = histories.last().unwrap();
    assert_ne!(last_history.operation.starts_with("10 / 0"), true);
}