# Bac à sable pour l'écriture de tests sur rust

## Installation 

En une seule ligne de commande : 

```bash
rustup update && \
cargo install cargo-binstall && \ 
cargo binstall cargo-llvm-cov --secure --no-confirm && \ 
cargo binstall cargo-nextest --secure --no-confirm && \ 
cargo binstall cargo-mutants --secure --no-confirm && \
cargo build
```

En détail : 

- `cargo-binstall` : binstall permet d'éviter de compiler des packages à installer en téléchargeant directement le binary
- `cargo-llvm-cov` : outils permettant de générer un rapport de test (avec entre autre le code coverage)
- `cargo-nextest` : improve le `cargo test` natif en améliorant entre autres grandement le temps d'exécution des tests (parallèlisme toussa)
- `cargo-mutants` : pour pouvoir effectuer des tests par mutation

Et pour désinstaller/nettoyer absolument tout ce qui a été installé au dessus : 

```bash
cargo uninstall cargo-binstall && \
cargo uninstall cargo-llvm-cov && \
cargo uninstall cargo-nextest && \
cargo uninstall cargo-mutants && \
cargo clean
```

## Démarrer le serveur web backend

- `cargo run`

## Exécution des tests 

### les tests unitaires

- `cargo ut`

Pour générer le code coverage : 

- `cargo utc`

### les tests par mutation

- `cargo mutants`