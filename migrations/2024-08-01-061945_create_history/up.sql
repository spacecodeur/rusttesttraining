-- Your SQL goes here
CREATE TABLE history (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    operation VARCHAR(50) NOT NULL
);